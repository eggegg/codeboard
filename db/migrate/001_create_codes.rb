class CreateCodes < ActiveRecord::Migration
  def self.up
    create_table :codes do |t|
      t.integer :user_id
      t.text :code
      t.string :title
      t.string :language
      t.string :url
      t.timestamps
    end
  end

  def self.down
    drop_table :codes
  end
end
