class Code < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  acts_as_taggable_on :tags
  
  validates_uniqueness_of :title
  validates_presence_of :title, :language, :code
  
  def to_url
    self.title.gsub(/ /,'-')
  end
end
