class CodeController < ApplicationController
  
  before_filter :login_required, :only => ['edit', 'create']
  
  def index
    redirect_to :action => "list"
  end

  def show
    @code = Code.find_by_url(params[:id])
    @title = @code.title
    if request.post?
      @comment = current_user.comments.build(params[:comment])
      @comment.code_id = @code.id
      if @comment.save
        flash[:notice] = "Successfully commented..."
        redirect_to :action => "show", :id => params[:id]
      else
        render :action => 'show'
      end
    end
  end

  def list
    @code = Code.paginate :page => params[:page], :order => 'updated_at DESC'
    @title = 'List'
  end

  def create
    @code = Code.new
    @title = 'Create'
    if request.post?
      @code = current_user.codes.build(params[:code])
      @code.tag_list = params[:tag]
      @code.url = @code.to_url
      if @code.save
        flash[:notice] = "Successfully created..."
        redirect_to :action => "show", :id => @code.url
      else
        render :action => 'create'
      end
    end
  end

  def edit
    @code = current_user.codes.find_by_url(params[:id])
    @title = "Edit::#{@code.title}"
    @tag = @code.tag_list
    if request.post?
      @code.update_attributes(params[:code])
      @code.tag_list = params[:tag]
      @code.url = @code.to_url
      @code.save!
      flash[:notice] = "Successfully edited..."
      redirect_to :action => "show", :id => @code.url
    end
  end

  def delete
    @code = current_user.codes.find_by_url(params[:id])
    @code.destroy
    flash[:notice] = "Successfully deleted..."
    redirect_to :action => "list"
  end
  
  def user
    @code = User.find_by_login(params[:id]).codes.paginate :page => params[:page], :order => 'updated_at DESC'
    @word = "Codes by #{params[:id]}"
    @title = "User::#{params[:id]}"
    render :action => 'list'
  end
  
  def tag
    @code = Code.find_tagged_with(params[:id], :on => :tags).paginate :page => params[:page], :order => 'updated_at DESC'
    @word = "Codes tagged with #{params[:id]}"
    @title = "Tag::#{params[:id]}"
    render :action => 'list'
  end
  
  def deletecomment
    @comment = current_user.comments.find(params[:id])
    @code = @comment.code
    @comment.destroy
    flash[:notice] = "Successfully deleted..."
    redirect_to :action => "show", :id => @code.url
  end
end
